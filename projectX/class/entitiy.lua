class "Entity"
{
	__init__ = function(self,spawnPosition,tileSheet,map)
		self.position = spawnPosition
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		self.tileSheet = tileSheet
		self.texWidth = 16
		self.texHeight = 16
		self.radius = 8
		self.imageOffset = {x= - self.radius , y = self.radius - self.texHeight}
		self.map = map
		self.elasticCoeficient= 0.25 -- strength of bounce in the dirction (point of impact -> centre of mass)
		self.cornerAsist = 0.125 -- added bounce at a right angle to the direction of travel to smooth out indirect impacts
		self.maxVel = 200  -- the rate at which velocity is effected by user input
		self.maxAcc = 500    -- cap on the velocity's magnitude
		self.drag = 0.8   -- rate at which speed is lost when not accelerating
		
		--Animations
		self.tileRow = 1
		self.tileColumn = 1
		self.orientation = 0
	end,
	
	draw = function(self)
		love.graphics.draw(self.tileSheet.image,self.tileSheet.quads[self.tileColumn][self.tileRow],self.position.x, self.position.y,math.rad(self.orientation),1,1,-self.imageOffset.x,-self.imageOffset.y)
	end,
	
	update = function(self, dt)
	
		if self.acceleration.x ~= 0 then
			self.velocity.x = self.velocity.x + self.acceleration.x * dt
		else
			self.velocity.x = self.velocity.x * self.drag
		end
		
		if self.acceleration.y ~= 0 then
			self.velocity.y = self.velocity.y + self.acceleration.y * dt
		else
			self.velocity.y = self.velocity.y * self.drag
		end
		
		local velMag = math.sqrt((self.velocity.x)^2 + (self.velocity.y)^2)
		if velMag > self.maxVel then
			self.velocity.x = self.velocity.x*(self.maxVel/velMag)
			self.velocity.y = self.velocity.y*(self.maxVel/velMag)
		end
		
	
		local containingGrid = self:getContianingGrid()
		for i=containingGrid.x -1,containingGrid.x +1,1 do
			for j=containingGrid.y -1,containingGrid.y +1,1 do
				if i > 0 and j > 0 and i <= #self.map.tiles and j <= #self.map.tiles[i] then
					self:checkColisionTile(self.map.tiles[i][j],containingGrid,dt)
				end
			end
		end 
		self.position.x = self.position.x + (self.velocity.x * dt)
		self.position.y = self.position.y + (self.velocity.y * dt)
	end,
	
	checkColisionTile = function(self,tile,containingGrid,dt)
		if not tile.solid then
			return
		--else print(tile.coords.x,tile.coords.y)
		end
		
		if containingGrid.x > tile.coords.x then
			self:checkColisionEdge(tile,"right",dt)
			if containingGrid.y < tile.coords.y and not (tile.coords.x==40 or self.map.tiles[tile.coords.x+1][tile.coords.y].solid) and not(tile.coords.y==1 or self.map.tiles[tile.coords.x][tile.coords.y-1].solid )  then
				self:checkColisionCorner({x=tile.coords.x*16,y=(tile.coords.y-1)*16},dt)
			elseif containingGrid.y > tile.coords.y and not (tile.coords.x==40 or self.map.tiles[tile.coords.x+1][tile.coords.y].solid) and not( tile.coords.y==30 or self.map.tiles[tile.coords.x][tile.coords.y+1].solid )then
				self:checkColisionCorner({x=tile.coords.x*16,y=(tile.coords.y)*16},dt)		
			end
		elseif containingGrid.x < tile.coords.x then
			self:checkColisionEdge(tile,"left",dt)
			if containingGrid.y < tile.coords.y and not (tile.coords.x==0 or self.map.tiles[tile.coords.x-1][tile.coords.y].solid) and not(tile.coords.y==1 or self.map.tiles[tile.coords.x][tile.coords.y-1].solid )then
				self:checkColisionCorner({x=(tile.coords.x-1)*16,y=(tile.coords.y-1)*16},dt)
			elseif containingGrid.y > tile.coords.y and not (tile.coords.x==0 or self.map.tiles[tile.coords.x-1][tile.coords.y].solid) and not( tile.coords.y==30 or self.map.tiles[tile.coords.x][tile.coords.y+1].solid )then
				self:checkColisionCorner({x=(tile.coords.x-1)*16,y=(tile.coords.y)*16},dt)		
			end
		end
		
		if containingGrid.y < tile.coords.y then
			self:checkColisionEdge(tile,"top",dt)
		elseif containingGrid.y > tile.coords.y then
			self:checkColisionEdge(tile,"bottom",dt)		
		end
	end,
	
	checkColisionEdge = function(self,tile,side,dt)
		if side == "bottom" then
			if tile.coords.x*16 > self.position.x + (self.velocity.x * dt) and  (tile.coords.x-1)*16 < self.position.x + (self.velocity.x * dt) then
				if tile.coords.y*16 > (self.position.y - self.radius) + (self.velocity.y * dt)  then
					self.position.y = tile.coords.y*16 + self.radius
					self.velocity.y = - self.velocity.y * self.elasticCoeficient
					self:wallColisionEvent()
				end
			end
		elseif side == "top" then
			if tile.coords.x*16 > self.position.x + (self.velocity.x * dt) and  (tile.coords.x-1)*16 < self.position.x + (self.velocity.x * dt) then
				if (tile.coords.y-1)*16 < (self.position.y + self.radius) + (self.velocity.y * dt)  then
					self.position.y = (tile.coords.y-1)*16 - self.radius
					self.velocity.y = - self.velocity.y * self.elasticCoeficient
					self:wallColisionEvent()
				end
			end
		elseif side == "right" then
			if tile.coords.y*16 > self.position.y + (self.velocity.y * dt) and  (tile.coords.y-1)*16 < self.position.y + (self.velocity.y * dt) then
				if tile.coords.x*16 > (self.position.x - self.radius) + (self.velocity.x * dt)  then
					self.position.x = tile.coords.x*16 + self.radius
					self.velocity.x = - self.velocity.x * self.elasticCoeficient
					self:wallColisionEvent()
					
				end
			end
		elseif side == "left" then
			if tile.coords.y*16 > self.position.y + (self.velocity.y * dt) and  (tile.coords.y-1)*16 < self.position.y + (self.velocity.y * dt) then
				if (tile.coords.x-1)*16 < (self.position.x + self.radius) + (self.velocity.x * dt)  then
					self.position.x = (tile.coords.x-1)*16 - self.radius
					self.velocity.x = - self.velocity.x * self.elasticCoeficient
					self:wallColisionEvent()
				end
			end
		end
		
		
		
	end,
	
	checkColisionCorner = function(self,corner,dt)
			local predictedPosition = {x= self.position.x +(self.velocity.x*dt),y= self.position.y +(self.velocity.y*dt)}
			local distancePredictedPositionToCorner = math.sqrt((corner.x - predictedPosition.x)^2+(corner.y -predictedPosition.y)^2) 
			if distancePredictedPositionToCorner > self.radius then
				return
			end
			
			local vecDirect = {x = corner.x - self.position.x , y = corner.y -self.position.y}
			local distance = math.sqrt(vecDirect.x^2 + vecDirect.y^2)
			local vectorMag =  math.sqrt((self.velocity.x*dt)^2 + (self.velocity.y*dt)^2)
			if vectorMag < (distance - self.radius) then
				return
			end
			local normalVect = {x =self.velocity.x/vectorMag, y =self.velocity.y/vectorMag}
			local dot = (normalVect.x * vecDirect.x)+(normalVect.y * vecDirect.y)
			if dot <= 0 then
				return
			end
			local F = distance^2 - dot^2
			if F >= self.radius^2 then
				return
			end
			local T = self.radius^2 - F
			if T < 0 then
				return
			end
			local distanceToColision = dot - math.sqrt(T) 
			if vectorMag < distanceToColision then
				return false
			end
			--local self.position = {x= self.position.x+(normalVect.x * distanceToColision)*dt, y= self.position.y+(normalVect.y*distanceToColision)*dt}
			self.position.x = self.position.x+(normalVect.x * distanceToColision)*dt
			self.position.y= self.position.y+(normalVect.y*distanceToColision)*dt
			local centerToImpact = {x = self.position.x -corner.x, y = self.position.y - corner.y}
			local normalcenterToImpact ={x= centerToImpact.x/self.radius,y = centerToImpact.y/self.radius}
			local speedOfimpact =  math.sqrt(self.velocity.x^2 + self.velocity.y^2)
			if math.abs(self.velocity.x) < math.abs(self.velocity.y) then

				self.velocity = {x= normalcenterToImpact.x*speedOfimpact*(self.elasticCoeficient+self.cornerAsist),y=normalcenterToImpact.y*speedOfimpact*self.elasticCoeficient}
			else
				self.velocity = {x=normalcenterToImpact.x*speedOfimpact*self.elasticCoeficient,y=normalcenterToImpact.y*speedOfimpact*(self.elasticCoeficient+self.cornerAsist)}
			end
			self:wallColisionEvent()
	end,
	
	wallColisionEvent = function(self)
	end,
	
	
	getContianingGrid = function(self)
		local x = math.ceil(self.position.x/16)
		local y = math.ceil(self.position.y/16)
		return {x = x,y=y}
	end
	
}