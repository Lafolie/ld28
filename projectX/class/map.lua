class "Map"
{
	__init__ = function(self, name)
		self.tiles = {}
		local tileData = love.filesystem.load("maps/" .. name)()
		for k, v in ipairs(tileData) do
			self.tiles[k] = {}
			for i, j in ipairs(v) do
				if j.quadRef.x == 4 and j.quadRef.y == 1 then
					j.stairs = "up"
				elseif j.quadRef.x == 3 and j.quadRef.y == 1 then
					j.stairs = "down"
				elseif j.quadRef.y == 4 or j.quadRef.y == 5 or (j.quadRef.x == 1 and j.quadRef.y ==1) then
					j.hole = true
				elseif (j.quadRef.x == 2 and j.quadRef.y == 1) or (j.quadRef.y == 9 and (j.quadRef.x == 2 or j.quadRef.x == 4))  then
					j.layer = 2
				else
					j.layer = 1
				end
				
				
				self.tiles[k][i] = Tile(j.coords, j.quadRef, j.solid, j.solidProjectile, j.layer, j.stairs, j.hole)
			end
		end

		-- for i=1,40,1 do
		-- 	self.tiles[i] = {}
		-- 	for j=1,30,1 do
		-- 		if (i == 10 and j ==10) or i ==1 or i==40 or j==1 or j==30 then 
		-- 			self.tiles[i][j] = Tile({x=i,y=j}, {x = 2, y = 1}, true)
		-- 		else
		-- 			self.tiles[i][j] = Tile({x=i,y=j}, {x = 1, y = 2}, false)
		-- 		end
		-- 	end
		-- end

		self.tileSheet = TileSheet("gfx/tiles.png",16)		
		self:createBatch()
		self.upStairs = self:getUpStairsTiles()
	end,
	
	draw = function(self)
		love.graphics.draw(self.batch1, 0, 0)
		love.graphics.draw(self.batch2, 0, 0)
	end,
	
	drawBatch1 = function(self)
		love.graphics.draw(self.batch1, 0, 0)
	end,
	
	drawBatch2 = function(self)
		love.graphics.draw(self.batch2, 0, 0)
	end,
	
	hideStairs = function(self)
		for j,x in ipairs(self.upStairs) do
			love.graphics.draw(self.tileSheet.image,self.tileSheet.quads[1][2],x.x,x.y)
		end
	end,
		
	
	
	createBatch = function(self)
		self.batch1 = love.graphics.newSpriteBatch(self.tileSheet.image, 1200)
		self.batch2 = love.graphics.newSpriteBatch(self.tileSheet.image, 1200)
		self.batch1:bind()
		self.batch2:bind() --gota love the spam
		for i,z in ipairs(self.tiles) do
			for j,x in ipairs(z) do
				local u, v = x.quadRef.x, x.quadRef.y
				if x.layer == 1 then
					self.batch1:add(self.tileSheet.quads[u][v], (i-1)*16, (j-1)*16)
				else
					self.batch2:add(self.tileSheet.quads[u][v], (i-1)*16, (j-1)*16)
				end
			end
		end
		self.batch1:unbind()
		self.batch2:unbind()
	end,
	
	getFloorTiles = function(self)
		local floorTiles = {}
		for i,z in ipairs(self.tiles) do
			for j,x in ipairs(z) do
				local u, v = x.quadRef.x, x.quadRef.y
				if u == 1 and v == 2 then
					table.insert(floorTiles,x)
				end
			end
		end
		return floorTiles
	end,
	
	getFlamesTiles = function(self)
		local flames = {}
		for i,z in ipairs(self.tiles) do
			for j,x in ipairs(z) do
				local u, v = x.quadRef.x, x.quadRef.y
				if u == 1 and v == 3 then
					table.insert(flames,{x= (x.coords.x*16)-8,y = (x.coords.y*16)-8})
				end
			end
		end
		return flames
	end,
	
	getUpStairsTiles = function(self)
		local stairs = {}
		for i,z in ipairs(self.tiles) do
			for j,x in ipairs(z) do
				local u, v = x.quadRef.x, x.quadRef.y
				if u == 4 and v == 1 then
					table.insert(stairs,{x= (x.coords.x*16)-16,y = (x.coords.y*16)-16})
				end
			end
		end
		return stairs
	end,

	saveToFile = function(self, fileName)
		local file = love.filesystem.newFile("maps/" .. fileName, "w")
		local concat = {"return {\n"}
			for i = 1, 40 do
				table.insert(concat, "\t{\n")
				for j = 1, 30 do
					table.insert(concat, "\t\t" .. self.tiles[i][j]:serialise())
				end
				table.insert(concat, "\t},\n")
			end
		table.insert(concat, "}")
		local saveString = table.concat(concat)
		local success = file:write(saveString)
		file:close()
		return success
	end
}