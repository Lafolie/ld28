class "TileSheet"
{
	__init__ = function(self, imagePath, tileSize)
		self.image = cache.image(imagePath)
		self.imageWidth = self.image:getWidth()
		self.imageHeight = self.image:getHeight()
		self.quads = {}
		
		for i=1,self.imageWidth/tileSize,1 do
			self.quads[i] = {}
			for j=1,self.imageHeight/tileSize,1 do
				self.quads[i][j] = love.graphics.newQuad(((i-1)*(tileSize+2))+1, ((j-1)*(tileSize+2))+1, tileSize, tileSize, self.imageWidth, self.imageHeight)
			end
		end
		
	end
}