require "class.entitiy"

class "Spear" (Entity)
{
	__init__ = function(self,spawnPosition,tileSheet,map)
		self.position = spawnPosition
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		self.tileSheet = tileSheet
		self.texWidth = 16
		self.texHeight = 16
		self.radius = 4
		self.imageOffset = {x= - self.radius , y = self.radius - self.texHeight}
		self.map = map
		self.elasticCoeficient= 1 -- strength of bounce in the dirction (point of impact -> centre of mass)
		self.cornerAsist = 0 -- added bounce at a right angle to the direction of travel to smooth out indirect impacts
		self.maxVel = 300  -- the rate at which velocity is effected by user input
		self.maxAcc = 500    -- cap on the velocity's magnitude
		self.drag = 1   -- rate at which speed is lost when not accelerating
		self.curl = 0		
		self.hitSfx = love.audio.newSource("sfx/collide.ogg", static)
		self.hitSfx:setPitch(0.95)
		self.hitSfx:setVolume(0.75)
		self.throwSfx = love.audio.newSource("sfx/throw.ogg", static)
		self.throwSfx:setVolume(0.5)
		self.throwSfx:play()
		
		--Animations
		self.tileRow = 1
		self.tileColumn = 1	
		self.orientation = 0
		self.rotationSpeed = 0
		
		self.destroy = false
		
		self.vectorToPlayer = {x=0,y=0}
		self.distanceToPlayer = {x=0,y=0}
		self.normalToPlayer = {x=0,y=0}	
	end,
		
	pointAtTarget = function(self)
		self.orientation = (math.atan2(self.normalToPlayer.y,self.normalToPlayer.x)/math.pi)*180 +90
	end,
		
	update = function(self, dt)
		Entity.update(self,dt)
	end,
	
	playerColisionEvent = function(self)
		self.hitSfx:stop()
		self.hitSfx:play()
		print("colision")
		self.destroy = true
	end,
	
	
	wallColisionEvent = function(self)
		self.hitSfx:stop()
		self.hitSfx:play()
		self.destroy = true
	end,
}