require "class.entitiy"

class "Enemy" (Entity)
{
	__init__ = function(self,spawnPosition,tileSheet,map)
		self.position = spawnPosition
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		self.tileSheet = tileSheet
		self.texWidth = 16
		self.texHeight = 16
		self.radius = 8
		self.imageOffset = {x= - self.radius , y = self.radius - self.texHeight}
		self.map = map
		self.elasticCoeficient= 0.25 -- strength of bounce in the dirction (point of impact -> centre of mass)
		self.cornerAsist = 0.125 -- added bounce at a right angle to the direction of travel to smooth out indirect impacts
		self.maxVel = 200  -- the rate at which velocity is effected by user input
		self.maxAcc = 500    -- cap on the velocity's magnitude
		self.drag = 0.9   -- rate at which speed is lost when not accelerating
		--self.HasBoomerang = true
		--self.boomerangTileSheet = TileSheet("gfx/boomerang.png",16)
		--self.boomerangSfx = cache.soundEffect("sfx/boomerang.ogg")
		--self.boomerangSfx:setLooping(true)
		--self.boomerang = Boomerang({x=spawnPosition.x,y=spawnPosition.y},self.boomerangTileSheet,map)
		--self.easyCatch = 16 -- lets you catch the boomerang from further away
		--self.autoCatch = false --[[ active to automatically catch boomerang when in range Cheat Mode!! Well done you found it 
		--you've Hacked the game here have a chocobo]]
		self.minRange = 32
		self.maxRange = 200
		self.vectorToPlayer = {x=0,y=0}
		self.distanceToPlayer = {x=0,y=0}
		self.normalToPlayer = {x=0,y=0}
		
		--Animations
		self.tileRow = 1
		self.tileColumn = 1
		self.orientation = 0
		
		self.timeTillChange = 2
		
		self.randomDirectionX = love.math.random(-1,1)
		self.randomDirectionY = love.math.random(-1,1)
		self.destroy = false
		
		self.throwSpear= false
		self.spearTime = love.math.random(1, 5)
		
		self.animationTimer = 0
	end,
	
	moveTowardsPlayer = function(self)
	
		if self.distanceToPlayer < self.maxRange and self.distanceToPlayer > self.minRange then
			self.acceleration.x = (self.normalToPlayer.x+self.randomDirectionX) * self.maxAcc  *  0.5
			self.acceleration.y = (self.normalToPlayer.y+self.randomDirectionY) * self.maxAcc * 0.5
		elseif self.distanceToPlayer < self.minRange then
			self.acceleration.x = (self.randomDirectionX - self.normalToPlayer.x) * self.maxAcc  *  0.5
			self.acceleration.y = (self.randomDirectionY - self.normalToPlayer.y) * self.maxAcc  *  0.5
		else
			self.acceleration.x = -self.velocity.x
			self.acceleration.y = -self.velocity.y
		end
	end,
	
	boomerangColisionEvent = function(self)
		self.destroy = true
	end,
	
	draw = function(self)
		Entity.draw(self)
		--self.boomerang:draw()
	end,
	
	update = function(self, dt)	
		self.timeTillChange = self.timeTillChange - dt
		self.spearTime = self.spearTime - dt
		if self.timeTillChange < 0 then
			self.timeTillChange = 2
			self.randomDirectionX = love.math.random(-100,100)/100
			self.randomDirectionY = love.math.random(-100,100)/100
			if self.distanceToPlayer < self.maxRange and self.distanceToPlayer > self.minRange and self.spearTime <= 0 then
				self.throwSpear = true
				self.spearTime = love.math.random(1, 5)
			end
		end

		Entity.update(self,dt)
		
		if math.abs(self.normalToPlayer.x) >  math.abs(self.normalToPlayer.y)then
			if self.normalToPlayer.x > 0 then
				self.tileColumn = 4
			else
				self.tileColumn = 3
			end
		else
			if self.normalToPlayer.y >= 0 then
				self.tileColumn = 1
			else
				self.tileColumn = 2
			end
			
		end	
		
	if self.acceleration.x == 0 and self.acceleration.y == 0 then
			self.tileRow = 1
		elseif self.animationTimer < 0.2 then
			self.tileRow = 1
		elseif self.animationTimer <0.4 then
			self.tileRow = 2
		elseif self.animationTimer <0.6 then
			self.tileRow = 1
		elseif self.animationTimer <0.8 then
			self.tileRow = 3
		else
			self.animationTimer = 0
		end
		
		self.animationTimer = self.animationTimer + dt	
	end,
	
}