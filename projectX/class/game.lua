--[[
	Game
	Entry point object. States also defined and loaded here.
]]

local stateList = {}

class "Game" 
{
	__init__ = function(self)
		self.state = "init" --todo: implement state system 
		
		--create gamestates
		local cs = function(s, ...)
			self:changeState(s, ...)
		end

		assert(love.filesystem.isDirectory("state"), "No states could be found!")
		local stateFiles = love.filesystem.getDirectoryItems("state")
		for _, file in ipairs(stateFiles) do
			local name = file:match("^(.+)%.lua$")
			if name then
				stateList[name] = Gamestate(name, file, cs)
			end
		end

		self:changeState("init")


		--initialise window
		local _, _, flags = love.window.getMode()
    	self.desktopWidth, self.desktopHeight = love.window.getDesktopDimensions(flags.display)
		love.graphics.setDefaultFilter("nearest", "nearest")
		love.window.setMode(1280, 960)
		self.scale = 2
	end,
	 
	update = function(self, dt, t)
		self.state:update(dt, t)
	end,
	
	draw = function(self)
		love.graphics.push()
		self.state:draw()
		love.graphics.pop()
		--love.graphics.setColor(255, 100, 100, 255)
		--love.graphics.print(collectgarbage("count")*1024, 1, 465)
	end,
	
	changeState = function(self, s, ...)
		print("state", s)
		assert(s and stateList[s], "Nil state specified.")
		self.state = stateList[s]
		self.state:reset(...)
	end,

	--löve callbacks
	keypressed = function(self, key, unicode)
		if key == "escape" then love.event.push("quit") end
		self.state:keypressed(key, unicode)
	end,
	
	keyreleased = function(self, key)
		self.state:keyreleased(key)
	end,
	
	mousepressed = function(self, x, y, button)
		self.state:mousepressed(x, y, button)
	end,
	
	mousereleased = function(self, x, y, button)
		self.state:mousereleased(x, y, button)
	end,
	
	mousefocus = function(self, f)
		self.state:mousefocus(f)
	end,
	
	textinput = function(self, text)
		self.state:textinput(text)
	end,
	
	joystickadded = function(self, joystick)
		self.state:joystickadded(joystick)
	end,
	
	joystickremoved = function(self, joystick)
		self.state:joystickremoved(joystick)
	end,
	
	gamepadpressed = function(self)
		self.state:gamepadpressed(joystick, button)
	end,
	
	gamepadreleased = function(self, joystick, button)
		self.state:gamepadreleased(joystick, button)
	end,
	
	gamepadaxis = function(self, joystick, axis)
		self.state:gamepadaxis(joystick, axis)
	end,
	
	focus = function(self, f)
		self.state:focus(f)
	end,
	
	visible = function(self, v)
		self.state:visible(v)
	end,
	
	quit = function(self)
		self.state:quit()
	end
}

--define gamestates
class "Gamestate" {
	__init__ = function(self, name, file, changeState)
		assert(name, "State declared with no name.")
		assert(not stateList[name], "State declared with ambiguous name.")
		assert(love.filesystem.exists("state/" .. file), "No state file found.")
		local stateSource = love.filesystem.load("state/" .. file)()
		for k, v in pairs(stateSource) do
			self[k] = v --assimilate source
		end
		
		local callbacks = {
			"reset", 
			"update",
			"draw",
			"keypressed",
			"keyreleased",
			"mousepressed",
			"mousereleased",
			"mousefocus",
			"textinput",
			"joystickadded",
			"joystickremoved",
			"gamepadpressed",
			"gamepadreleased",
			"gamepadaxis",
			"focus",
			"visible",
			"quit"
		}
		for _, cb in ipairs(callbacks) do
			self[cb] = self[cb] or function() end --default callbacks for stability
		end	

		self.changeState = changeState
	end
}
	 