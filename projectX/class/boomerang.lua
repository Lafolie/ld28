require "class.entitiy"

class "Boomerang" (Entity)
{
	__init__ = function(self,spawnPosition,tileSheet,map)
		self.position = spawnPosition
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		self.tileSheet = tileSheet
		self.texWidth = 16
		self.texHeight = 16
		self.radius = 8
		self.imageOffset = {x= - self.radius , y = self.radius - self.texHeight}
		self.map = map
		self.elasticCoeficient= 1 -- strength of bounce in the dirction (point of impact -> centre of mass)
		self.cornerAsist = 0 -- added bounce at a right angle to the direction of travel to smooth out indirect impacts
		self.maxVel = 300  -- the rate at which velocity is effected by user input
		self.maxAcc = 800    -- cap on the velocity's magnitude
		self.drag = 1   -- rate at which speed is lost when not accelerating
		self.curl = 0
		self.bounces = 0		
		self.hitSfx = cache.soundEffect("sfx/collide.ogg")
		self.onFloor = false
		self.HitsBeforeFall = 4
		self.atract = false
		
		--Animations
		self.tileRow = 1
		self.tileColumn = 1	
		self.orientation = 0
		self.maxRotationSpeed = 1080
		self.rotationSpeed = 1080
		
		self.rotationLossRate = 100
		self.rotationLossOnHit = 50
		self.fallLimit  = 360
		
		self.screenBounds = {x=love.graphics.getWidth(),y=love.graphics.getHeight()}
		
		self.notOnLevel = false
		self.onCurrentLevel = true

	end,
	
	mousepressed = function(self, x, y, button)
		
	end,
	
	resetSpin = function(self)
		self.rotationSpeed = self.maxRotationSpeed
	end,
	
	checkColisionTile = function(self,tile,containingGrid,dt)
		if not tile.solidProjectile then
			return
		--else print(tile.coords.x,tile.coords.y)
		end
		
		if containingGrid.x > tile.coords.x then
			self:checkColisionEdge(tile,"right",dt)
			if containingGrid.y < tile.coords.y and not (tile.coords.x==40 or self.map.tiles[tile.coords.x+1][tile.coords.y].solidProjectile) and not(tile.coords.y==1 or self.map.tiles[tile.coords.x][tile.coords.y-1].solidProjectile )  then
				self:checkColisionCorner({x=tile.coords.x*16,y=(tile.coords.y-1)*16},dt)
			elseif containingGrid.y > tile.coords.y and not (tile.coords.x==40 or self.map.tiles[tile.coords.x+1][tile.coords.y].solidProjectile) and not( tile.coords.y==30 or self.map.tiles[tile.coords.x][tile.coords.y+1].solidProjectile )then
				self:checkColisionCorner({x=tile.coords.x*16,y=(tile.coords.y)*16},dt)		
			end
		elseif containingGrid.x < tile.coords.x then
			self:checkColisionEdge(tile,"left",dt)
			if containingGrid.y < tile.coords.y and not (tile.coords.x==0 or self.map.tiles[tile.coords.x-1][tile.coords.y].solidProjectile) and not(tile.coords.y==1 or self.map.tiles[tile.coords.x][tile.coords.y-1].solidProjectile )then
				self:checkColisionCorner({x=(tile.coords.x-1)*16,y=(tile.coords.y-1)*16},dt)
			elseif containingGrid.y > tile.coords.y and not (tile.coords.x==0 or self.map.tiles[tile.coords.x-1][tile.coords.y].solidProjectile) and not( tile.coords.y==30 or self.map.tiles[tile.coords.x][tile.coords.y+1].solidProjectile )then
				self:checkColisionCorner({x=(tile.coords.x-1)*16,y=(tile.coords.y)*16},dt)		
			end
		end
		
		if containingGrid.y < tile.coords.y then
			self:checkColisionEdge(tile,"top",dt)
		elseif containingGrid.y > tile.coords.y then
			self:checkColisionEdge(tile,"bottom",dt)		
		end
	end,
	
	update = function(self, dt)
	--print(self.position.x,self.position.y,self.velocity.x,self.velocity.y,self.bounces)

		
		if not self.onFloor then
			self.orientation = self.orientation + self.rotationSpeed * dt
			if self.orientation > 360 then
				self.orientation = self.orientation - 360
			end
			
			if not self.atract then
				self.acceleration.x = self.velocity.y * self.curl
				self.acceleration.y = -self.velocity.x * self.curl
			end
			Entity.update(self,dt)
		--else
		--	if self.position.x <= 16 or self.position.x >= self.screenBounds.x -16 or self.position.y <= 16 or self.position.y >= self.screenBounds.y - 16 then
		--		self:outTheWindowEvent()
		--	end
		end
	end,
	
	enemyColisionEvent = function(self)
		self.hitSfx:stop()
		self.hitSfx:play()
		self.curl = - self.curl
		--self.bounces = self.bounces +1
		print("collision")
		self.rotationSpeed = -self.rotationSpeed
	end,
	
	outTheWindowEvent = function(self)
		print("oops I've lost me boomerang")
	end,
	
	
	wallColisionEvent = function(self)
		self.hitSfx:stop()
		self.hitSfx:play()
		self.curl = - self.curl
		self.bounces = self.bounces +1
		self.rotationSpeed = self.rotationLossOnHit*(math.abs(self.rotationSpeed)/self.rotationSpeed) -self.rotationSpeed
	end,
}