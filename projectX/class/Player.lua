require "class.entitiy"

class "Player" (Entity)
{
	__init__ = function(self,spawnPosition,tileSheet,map)
		self.position = spawnPosition
		self.velocity = {x=0,y=0}
		self.acceleration = {x=0,y=0}
		self.tileSheet = tileSheet
		self.texWidth = 16
		self.texHeight = 16
		self.radius = 7
		self.imageOffset = {x= - self.radius , y = self.radius - self.texHeight}
		self.map = map
		self.elasticCoeficient= 0.25 -- strength of bounce in the dirction (point of impact -> centre of mass)
		self.cornerAsist = 0.125 -- added bounce at a right angle to the direction of travel to smooth out indirect impacts
		self.maxVel = 200  -- the rate at which velocity is effected by user input
		self.maxAcc = 500    -- cap on the velocity's magnitude																		-- dificulty change, increases maxStrength of right click pull for more control required to complete harder puzzles
		self.drag = 0.8   -- rate at which speed is lost when not accelerating
		self.HasBoomerang = true
		self.boomerangTileSheet = TileSheet("gfx/boomerang.png",16)
		self.boomerangSfx = cache.soundEffect("sfx/boomerang.ogg")
		self.boomerangSfx:setLooping(true)
		self.boomerang = Boomerang({x=spawnPosition.x,y=spawnPosition.y},self.boomerangTileSheet,map)
		self.easyCatch = 16 -- lets you catch the boomerang from further away
		self.autoCatch = false --[[ active to automatically catch boomerang when in range Cheat Mode!! Well done you found it 
		you've Hacked the game here have a chocobo]]
		
		self.hp = 8
		--Animations
		self.tileRow = 1
		self.tileColumn = 1
		self.orientation = 0

		--sounds
		self.footSteps = cache.soundEffect("sfx/footsteps.ogg")
		self.footSteps:setLooping(true)
		self.footSteps:setVolume(0.5)
		self.ouch = cache.soundEffect("sfx/ouch2.ogg")
		self.ouch:setVolume(0.9)
		--(coords, img, blendMode, life, direction, rate, scaleA, scaleB, speedA, speedB, spread, rotation)
		--particles
		self.moveDust = Particle(self.position, "gfx/dust.png", "alpha", 0.25, 0, 10, 2, 3, 10, 5, 5, 20)
		self.sparkles = Particle(self.boomerang.position, "gfx/sparkle.png", "additive", 1, math.rad(-90), 3, 1, 0.1, 25, 1, 1, 45)
		
		self.animationTimer = 0
		
		self.scale = 2
		
	end,
	
	calculateVectorToPlayer = function(self,location)
		local vectorToPlayer = {x = self.position.x - location.x,y=self.position.y - location.y}
		return vectorToPlayer
	end,
	
	gotHitEvent = function(self)
		self.hp = self.hp - 1
		self.ouch:stop()
		self.ouch:play()
		self.flash = 128
	end,
	
	mousepressed = function(self, x, y, button)
		if self.boomerang.onCurrentLevel == false then
			return
		end
		if button == "l" then
			if self.HasBoomerang then
				local vectorMouse = {x = x/game.scale - self.position.x , y = y/game.scale -self.position.y}
				local distance = math.sqrt(vectorMouse.x^2 + vectorMouse.y^2)
				local normalThrow = {x=vectorMouse.x/distance,y=vectorMouse.y/distance}
				self.boomerang.velocity.x = normalThrow.x*self.boomerang.maxVel
				self.boomerang.velocity.y = normalThrow.y*self.boomerang.maxVel	
				self.boomerang:resetSpin()
				self.HasBoomerang = false
			else
				local vectorToBoomerang = {x= self.position.x - self.boomerang.position.x,y= self.position.y - self.boomerang.position.y}
				local distance = math.sqrt(vectorToBoomerang.x^2 + vectorToBoomerang.y^2)
				if distance < self.radius + self.boomerang.radius + self.easyCatch then
					self.boomerang.velocity.x = 0
					self.boomerang.velocity.y = 0
					self.boomerang.position.x = self.position.x
					self.boomerang.position.y = self.position.y
					self.HasBoomerang = true
					self.boomerang.onFloor = false
					self.boomerang.bounces = 0
					self.boomerang.orientation = 0
				end
			end
		end
	end,
		
	draw = function(self)
		self.moveDust:draw()
		
		if self.boomerang.onCurrentLevel then
			if self.tileColumn== 2 or self.tileColumn== 4 then
				self.boomerang:draw()
				self.sparkles:draw()
				Entity.draw(self)
			else
				Entity.draw(self)
				self.boomerang:draw()
				self.sparkles:draw()
			end
		else
			Entity.draw(self)
		end
		
		
		
		love.graphics.setColor(255, 255, 255, 255)
		if self.flash then
			love.graphics.setColor(255, 50, 50, self.flash)
			love.graphics.setBlendMode("additive")
			love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
			love.graphics.setBlendMode("alpha")
			love.graphics.setColor(255, 255, 255, 255)
		end

		love.graphics.print(self.boomerang.position.x, 1, 16)
		love.graphics.print(self.boomerang.position.y, 1, 32)
	end,
	
	update = function(self, dt)
		if love.keyboard.isDown("right", "d") then
			self.acceleration.x = self.maxAcc
		elseif love.keyboard.isDown("left","a") then
			self.acceleration.x = -self.maxAcc
		else
			self.acceleration.x = 0
		end
			
		if love.keyboard.isDown("up", "w") then
			self.acceleration.y = -self.maxAcc
		elseif love.keyboard.isDown("down","s") then
			self.acceleration.y = self.maxAcc
		else
			self.acceleration.y = 0
		end
		
		if self.acceleration.x ~= 0 or self.acceleration.y ~= 0 then
			self.footSteps:play()
			self.moveDust:start()
		else
			self.footSteps:stop()
			self.moveDust:stop()
		end

		--update particles
		self.moveDust:update(dt, self.position)
		self.sparkles:update(dt, self.boomerang.position)
		self.flash = self.flash and self.flash - 300 * dt
		if self.flash and self.flash <= 0 then self.flash = nil end

		Entity.update(self,dt)
		
		local containingGrid = self:getContianingGrid()
		if self.map.tiles[containingGrid.x][containingGrid.y].stairs ~= self.lastStairs and self.lastStairs == false then
			print(self.map.tiles[containingGrid.x][containingGrid.y].stairs)
			self.switchMap = true
		end
		self.lastStairs = self.map.tiles[containingGrid.x][containingGrid.y].stairs
		
		
		if self.HasBoomerang then
			self.boomerangSfx:stop()
			self.sparkles:stop()
			self.boomerang.position.x = self.position.x
			self.boomerang.position.y = self.position.y
		elseif self.boomerang.onFloor then
			self.sparkles.system:setEmissionRate(3)
			self.boomerangSfx:stop()
		elseif math.abs(self.boomerang.rotationSpeed) < self.boomerang.fallLimit then
			local boomerangContainingGrid = self.boomerang:getContianingGrid()
			self.boomerang.onFloor = true
			self.boomerang.velocity.x = 0
			self.boomerang.velocity.y = 0	
			self.boomerang.acceleration.x = 0
			self.boomerang.acceleration.y = 0
			if self.boomerang.position.x <= 16 or self.boomerang.position.x >= self.boomerang.screenBounds.x/game.scale -16 or self.boomerang.position.y <= 16 or self.boomerang.position.y >= self.boomerang.screenBounds.y/game.scale - 16 then
				self.boomerang:outTheWindowEvent()
			elseif self.map.tiles[boomerangContainingGrid.x][boomerangContainingGrid.y].hole then  --rearanged if statement to prevent error when boomerang falls outside of map limits
				print("wheres it gone")
				self.recordBoomerangPosition = true
				self.boomerang.onCurrentLevel = false
			end
		else
			self.boomerang.rotationSpeed = self.boomerang.rotationSpeed - (dt*self.boomerang.rotationLossRate*(math.abs(self.boomerang.rotationSpeed)/self.boomerang.rotationSpeed))
			print(self.boomerang.rotationSpeed)
			self.sparkles:start()
			self.boomerangSfx:play()
			self.boomerang:update(dt)
			self.sparkles.system:setEmissionRate(math.abs(self.boomerang.rotationSpeed / 24))
		end
		
		if self.autoCatch and self.boomerang.bounces > 0 then
			local vectorToBoomerang = {x= self.position.x - self.boomerang.position.x,y= self.position.y - self.boomerang.position.y}
			local distance = math.sqrt(vectorToBoomerang.x^2 + vectorToBoomerang.y^2)
			if distance < self.radius + self.boomerang.radius + self.easyCatch then
				self.boomerang.velocity.x = 0
				self.boomerang.velocity.y = 0
				self.boomerang.position.x = self.position.x
				self.boomerang.position.y = self.position.y
				self.HasBoomerang = true
				self.boomerang.onFloor = false
				self.boomerang.bounces = 0
				self.boomerang.orientation = 0
			end
		end
		
		if love.mouse.isDown("r") and self.boomerang.rotationSpeed < 1060 then -- dificulty change, lets pull kick in sooner for more control required to complete harder puzzles
			self.boomerang.atract = true
			local vectorToBoomerang = {x= self.position.x - self.boomerang.position.x,y= self.position.y - self.boomerang.position.y}
			local distance = math.sqrt(vectorToBoomerang.x^2 + vectorToBoomerang.y^2)
			if distance > 16 then
				self.boomerang.acceleration.x = (vectorToBoomerang.x/distance)* self.boomerang.maxAcc 
				self.boomerang.acceleration.y = (vectorToBoomerang.y/distance)* self.boomerang.maxAcc
			end
		else
			self.boomerang.atract = false
		end
     
		
		local vectorMouse = {x = love.mouse.getX()/game.scale - self.position.x , y = love.mouse.getY()/game.scale -self.position.y}
		if math.abs(vectorMouse.x) >  math.abs(vectorMouse.y)then
			if vectorMouse.x > 0 then
				self.tileColumn = 4
				if self.HasBoomerang then
					self.boomerang.orientation = 40
					self.boomerang.imageOffset.x = -4
					self.boomerang.imageOffset.y = -18
				end
			else
				self.tileColumn = 3
				if self.HasBoomerang then
					self.boomerang.orientation = 220
					self.boomerang.imageOffset.x = 0
					self.boomerang.imageOffset.y = -8
				end
			end
		else
			if vectorMouse.y >= 0 then
				self.tileColumn = 1
				if self.HasBoomerang then
					self.boomerang.orientation = 40
					self.boomerang.imageOffset.x = -4
					self.boomerang.imageOffset.y = -18
				end
			else
				self.tileColumn = 2
				if self.HasBoomerang then
					self.boomerang.orientation = 180
					self.boomerang.imageOffset.x = -4
					self.boomerang.imageOffset.y = -6
				end
			end
			
		end
		
		if not self.HasBoomerang then
			self.boomerang.imageOffset.x = -8
			self.boomerang.imageOffset.y = -8
		end
		
		if self.acceleration.x == 0 and self.acceleration.y == 0 then
			self.tileRow = 1
		elseif self.animationTimer < 0.2 then
			self.tileRow = 1
		elseif self.animationTimer <0.4 then
			self.tileRow = 2
		elseif self.animationTimer <0.6 then
			self.tileRow = 1
		elseif self.animationTimer <0.8 then
			self.tileRow = 3
		else
			self.animationTimer = 0
		end
		
		self.animationTimer = self.animationTimer + dt	

			
	end
}