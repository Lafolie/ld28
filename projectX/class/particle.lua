class "Particle" {
	__init__ = function(self, coords, img, blendMode, life, direction, rate, scaleA, scaleB, speedA, speedB, spread, rotation)
		self.img = cache.image(img)
		self.system = love.graphics.newParticleSystem(self.img, 256)
		self.system:setPosition(coords.x, coords.y)
		self.blendMode = blendMode
		self.system:setParticleLifetime(life)
		self.system:setDirection(direction)
		self.system:setEmissionRate(rate)
		self.system:setSizes(scaleA, scaleB)
		self.system:setSpeed(speedA, speedB)
		self.system:setSpread(spread)
		self.system:setRotation(rotation)
		self.system:setInsertMode("random")
	end,

	update = function(self, dt, newCoords)
		if newCoords then
			self.system:setPosition(newCoords.x, newCoords.y)
		end

		self.system:update(dt)
	end,

	draw = function(self)
		love.graphics.setBlendMode(self.blendMode)
		love.graphics.draw(self.system, 0, 0)
		love.graphics.setBlendMode("alpha")
	end,

	pause = function(self)
		self.system:pause()
	end,

	start = function(self)
		self.system:start()
	end,

	stop = function(self)
		self.system:stop()
	end
}