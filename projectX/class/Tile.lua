class "Tile"
{
	__init__ = function(self,coords, quadRef, solid, solidProjectile, layer, stairs, hole)
		self.coords = coords
		self.solid = solid
		self.solidProjectile = solidProjectile
		self.layer = layer
		self.stairs = stairs
		self.hole = hole
		self.quadRef = quadRef or {x = 1, y = 1}
	end,

	setProperties = function(self, p)
		self.solid = p.solid
		self.solidProjectile = p.solidProjectile
		self.layer = p.layer
		self.stairs = p.stairs
		self.hole = p.hole --phole
	end,

	serialise = function(self)
		local concat = {"{", }
		for k, v in pairs(self) do
			if type(v) == "table" then v = "{x = " .. v.x .. ", y = " .. v.y .. "}" end
			table.insert(concat, k .. " = " .. tostring(v) ..", ")
		end
		table.insert(concat, "},\n")
		return table.concat(concat)
	end
}