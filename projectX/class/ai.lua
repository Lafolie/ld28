class "AI"
{
	__init__ = function(self,map,player,level)
		self.level = level
		self.map = map
		self.player = player
		self.enemies = {}
		self.spears = {}
		self.enemyTileSheet = TileSheet("gfx/maskMan.png",16)
		self.spearTileSheet = TileSheet("gfx/spear.png",16)
		self.bossTileSheet = TileSheet("gfx/maskMan.png",16)
		--self:addBoss({x=300,y=300})
		--self:addEnemy({x=100,y=100})
		--self:addEnemy({x=300,y=100})
		--self:addEnemy({x=100,y=300})
		self.victory = false
		self.switchedBoomerangs = false
		self.bossLevel = 10
		self:spawnEnimies()

	end,
	
	spawnEnimies = function(self)
		if self.level ~= self.bossLevel then
			local floorTiles = self.map:getFloorTiles()
			print(#floorTiles)
			local numberOfEnimies = self.level
			for i,z in ipairs(floorTiles) do
				if love.math.random(0,200) < numberOfEnimies then
					assert(z.coords.x == z.coords.x, "UGSFDIUYSGFUYSDGFUYSDG")
					--self:addEnemy({x=z.coords.x *16,y=z.coords.y*16})
					self:addEnemy({x=(z.coords.x *16)-8,y=(z.coords.y*16)-8}) -- fixed WOOOOOOOOOOOOO!!!!!!
				end	
			end
		else
			self:addBoss({x=300,y=300})
		end
		
	end,
	
	addEnemy = function(self,spawnLocation)
		table.insert(self.enemies,Enemy(spawnLocation,self.enemyTileSheet,self.map))
	end,
	
	addBoss = function(self,spawnLocation)
		table.insert(self.enemies,Boss(spawnLocation,self.bossTileSheet,self.map))
	end,
	
	
	addSpear= function(self,spawnLocation)
		local spear = Spear(spawnLocation,self.spearTileSheet,self.map)
		table.insert(self.spears,spear)
		self:updateVectorToPlayer(spear)
		spear.velocity.x = spear.normalToPlayer.x *spear.maxVel
		spear.velocity.y = spear.normalToPlayer.y *spear.maxVel
		spear:pointAtTarget()
	end,
	
	draw = function(self)
		--self.enemy:draw()
		for i = #self.enemies, 1, -1 do
			local enemy = self.enemies[i]
			enemy:draw()
		end
		
		for i = #self.spears, 1, -1 do
			local spear = self.spears[i]
			spear:draw()
		end
	end,
	
	updateVectorToPlayer = function(self,enemy)
		enemy.vectorToPlayer = self.player:calculateVectorToPlayer(enemy.position)
		enemy.distanceToPlayer = math.sqrt(enemy.vectorToPlayer.x^2 + enemy.vectorToPlayer.y^2)
		enemy.normalToPlayer = {x=enemy.vectorToPlayer.x/enemy.distanceToPlayer,y=enemy.vectorToPlayer.y/enemy.distanceToPlayer}
	end,
	
	checkColisionsBoomerangVsEnemy = function(self,enemy,dt)

		local predictedPosition = {x= self.player.boomerang.position.x +(self.player.boomerang.velocity.x*dt),y= self.player.boomerang.position.y +(self.player.boomerang.velocity.y*dt)}
		local distancePredictedPositionToCorner = math.sqrt((enemy.position.x - predictedPosition.x)^2+(enemy.position.y -predictedPosition.y)^2) 
		if distancePredictedPositionToCorner > self.player.boomerang.radius + enemy.radius then
			return
		end
		
		if self.player.HasBoomerang then
			return 
		end 
		if self.player.boomerang.onFloor then
			return 
		end
		local vecDirect = {x = enemy.position.x - self.player.boomerang.position.x , y = enemy.position.y -self.player.boomerang.position.y}
		local distance = math.sqrt(vecDirect.x^2 + vecDirect.y^2)
		local vectorMag =  math.sqrt((self.player.boomerang.velocity.x*dt)^2 + (self.player.boomerang.velocity.y*dt)^2)
		if vectorMag < (distance - (self.player.boomerang.radius + enemy.radius)) then
			return
		end
		local normalVect = {x =self.player.boomerang.velocity.x/vectorMag, y =self.player.boomerang.velocity.y/vectorMag}
		local dot = (normalVect.x * vecDirect.x)+(normalVect.y * vecDirect.y)
		if dot <= 0 then
			return
		end
		local F = distance^2 - dot^2
		if F >= (self.player.boomerang.radius + enemy.radius)^2 then
			return
		end
		local T = (self.player.boomerang.radius + enemy.radius)^2 - F
		if T < 0 then
			return
		end
		local distanceToColision = dot - math.sqrt(T) 
		if vectorMag < distanceToColision then
			return
		end
		local boomerangPosition = {x= self.player.boomerang.position.x+(normalVect.x * distanceToColision)*dt, y= self.player.boomerang.position.y+(normalVect.y*distanceToColision)*dt}			
		local centerToImpact = {x = boomerangPosition.x - enemy.position.x, y = boomerangPosition.y -  enemy.position.y}
		local normalcenterToImpact ={x= centerToImpact.x/(self.player.boomerang.radius + enemy.radius),y = centerToImpact.y/(self.player.boomerang.radius + enemy.radius)}
		local speedOfimpact =  math.sqrt(self.player.boomerang.velocity.x^2 + self.player.boomerang.velocity.y^2)
		if math.abs(self.player.boomerang.velocity.x) < math.abs(self.player.boomerang.velocity.y) then

			self.player.boomerang.velocity = {x= normalcenterToImpact.x*speedOfimpact*(self.player.boomerang.elasticCoeficient+self.player.boomerang.cornerAsist),y=normalcenterToImpact.y*speedOfimpact*self.player.boomerang.elasticCoeficient}
		else
			self.player.boomerang.velocity = {x=normalcenterToImpact.x*speedOfimpact*self.player.boomerang.elasticCoeficient,y=normalcenterToImpact.y*speedOfimpact*(self.player.boomerang.elasticCoeficient+self.player.boomerang.cornerAsist)}
		end
		print("hit")
		--self:wallColisionEvent()
		enemy:boomerangColisionEvent()
		self.player.boomerang:enemyColisionEvent()

	end,
	
	checkColisionsBossBoomerangVsPlayer = function(self,boss,dt)
			if boss.HasBoomerang then
				return false
			end 
			if boss.boomerang.onFloor then
				return false
			end
			local vecDirect = {x = self.player.position.x - boss.boomerang.position.x , y = self.player.position.y -boss.boomerang.position.y}
			local distance = math.sqrt(vecDirect.x^2 + vecDirect.y^2)
			local vectorMag =  math.sqrt((boss.boomerang.velocity.x*dt)^2 + (boss.boomerang.velocity.y*dt)^2)
			if vectorMag < (distance - (boss.boomerang.radius + self.player.radius)) then
				return
			end
			local normalVect = {x =boss.boomerang.velocity.x/vectorMag, y =boss.boomerang.velocity.y/vectorMag}
			local dot = (normalVect.x * vecDirect.x)+(normalVect.y * vecDirect.y)
			if dot <= 0 then
				return
			end
			local F = distance^2 - dot^2
			if F >= (boss.boomerang.radius + self.player.radius)^2 then
				return
			end
			local T = (boss.boomerang.radius + self.player.radius)^2 - F
			if T < 0 then
				return
			end
			local distanceToColision = dot - math.sqrt(T) 
			if vectorMag < distanceToColision then
				return false
			end
			local boomerangPosition = {x= boss.boomerang.position.x+(normalVect.x * distanceToColision)*dt, y= boss.boomerang.position.y+(normalVect.y*distanceToColision)*dt}			
			local centerToImpact = {x = boomerangPosition.x - self.player.position.x, y = boomerangPosition.y -  self.player.position.y}
			local normalcenterToImpact ={x= centerToImpact.x/(boss.boomerang.radius + self.player.radius),y = centerToImpact.y/(boss.boomerang.radius + self.player.radius)}
			local speedOfimpact =  math.sqrt(boss.boomerang.velocity.x^2 + boss.boomerang.velocity.y^2)
			if math.abs(boss.boomerang.velocity.x) < math.abs(boss.boomerang.velocity.y) then

				boss.boomerang.velocity = {x= normalcenterToImpact.x*speedOfimpact*(boss.boomerang.elasticCoeficient+boss.boomerang.cornerAsist),y=normalcenterToImpact.y*speedOfimpact*boss.boomerang.elasticCoeficient}
			else
				boss.boomerang.velocity = {x=normalcenterToImpact.x*speedOfimpact*boss.boomerang.elasticCoeficient,y=normalcenterToImpact.y*speedOfimpact*(boss.boomerang.elasticCoeficient+boss.boomerang.cornerAsist)}
			end
			print("hit")
			--self:wallColisionEvent()
			self.player:gotHitEvent()
			boss.boomerang:enemyColisionEvent()
	end,
	
	update = function(self, dt)
		for i = #self.enemies, 1, -1 do
			local enemy = self.enemies[i]
			self:updateVectorToPlayer(enemy)
			if not self.victory then
				enemy:moveTowardsPlayer()

			end

			enemy:update(dt)
			
			self:checkColisionsBoomerangVsEnemy(enemy,dt)
			if enemy.destroy then
				table.remove(self.enemies,i)
			end
			if enemy.throwSpear then
				enemy.throwSpear= false
				self:addSpear({x = enemy.position.x,y= enemy.position.y})
			end
			
			if self.switchedBoomerangs == false and enemy.boomerang and self.victory then
					if self.player.HasBoomerang then
						self.player.boomerang.velocity.x = self.player.boomerang.maxVel
						self.player.boomerang.velocity.y = 0
						self.player.HasBoomerang = false
					end
					
					local golden = enemy.boomerang
					local purple = self.player.boomerang
					golden.rotationLossRate = 0
					golden.rotationLossOnHit = 0
					self.player.boomerang = golden
					enemy.boomerang = purple
					self.switchedBoomerangs = true
					enemy.destroy = true
			end
			
			
			
			if enemy.boomerang and not enemy.HasBoomerang then
				if enemy.hp == 0 then
					self.victory = true
				end
				self:checkColisionsBossBoomerangVsPlayer(enemy,dt)
			end
		end
		for i = #self.spears, 1, -1 do
			local spear = self.spears[i]
			self:updateVectorToPlayer(spear)
			if spear.distanceToPlayer < (spear.radius + self.player.radius) then
				spear:playerColisionEvent()
				self.player:gotHitEvent()
			end
			spear:update(dt)
			--self:checkColisionsBoomerangVsEnemy(enemy,dt)
			if spear.destroy then
				table.remove(self.spears,i)
			end
		end
	end
	
}