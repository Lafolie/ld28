return {
	reset = function(self)
		print "init state"
	end,

	update = function(self, dt)
		-- self.changeState("world")
	end,

	draw = function(self)
		love.graphics.print("Push any key to start.", 1, 1)
	end,
	
	keypressed = function(self, key, unicode)
		if key == "tab" then
			self.changeState("edit")
		else
			self.changeState("world")
		end
	end,

	mousepressed = function(self, x, y, button)
		self.changeState("world")
	end
}