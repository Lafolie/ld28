--[[
	Editor reqs:
		-solid to player
		-solid to projectile
		-draw order
		-stairs
		-eats boomerang
]]

local inside = function(x, y, ax, ay, aw, ah) 
	local r =   x > ax and
				x < ax + aw and
				y > ay and
				y < ay + ah
	return r
end

local modKey = function()
	return love.keyboard.isDown("lctrl") or love.keyboard.isDown("lgui")
end


return {
	reset = function(self)
	love.window.setTitle("Level Editor")
	love.window.setMode(640, 480)

	self.settings = {solid = true, solidProjectile = true, layer = 1, stairs = false, hole = false}
	local editableProperties = {
		{
			x = 75,
			y = 35,
			text = "Player: ",
			value = "solid",
			click = function(self, settings)
				settings[self.value] = not settings[self.value]
			end
		},
		{
			x = 75,
			y = 50,
			text = "Missile: ",
			value = "solidProjectile",
			click = function(self, settings)
				settings[self.value] = not settings[self.value]
			end
		},
		{
			x = 75,
			y = 65,
			text = "Layer: ",
			value = "layer",
			click = function(self, settings)
				settings[self.value] = settings[self.value] == 1 and 2 or 1
			end
		},
		{
			x = 75,
			y = 80,
			text = "Stairs: ",
			value = "stairs",
			click = function(self, settings)
				settings[self.value] = not settings[self.value]
			end
		},
		{
			x = 75,
			y = 95,
			text = "Hole: ",
			value = "hole",
			click = function(self, settings)
				settings[self.value] = not settings[self.value]
			end
		}
	}

	self.gui = {x = 0, y = 0, w = 150, h = 240, pad = 1, widget = editableProperties}
	self.cursor = {x = 0, y = 0}
	self.paintTile = {x = 1, y = 1}
	self.previousTile = {}
	self.log = {{text = "Press Tab for contextual menu.", timeLeft = 500}}
	--self.logHistory = {}

	--map & tiles
	self.mapList = {}
	self.currentMapID = 1
	local ls = love.filesystem.getDirectoryItems("maps")
	for _, file in ipairs(ls) do
		local name = file:match("^(.+)%.map$")
		if name  and name ~= "boss" then
			table.insert(self.mapList, file)
			print(_, name)
		end
	end
	self.map = Map(self.mapList[self.currentMapID])
	self.tileImg = self.map.tileSheet.image
	self:logMsg(self.mapList[self.currentMapID])
	self:logMsg(#self.mapList .. " maps found.")

	--graphical thingies
	love.graphics.setBackgroundColor(128, 128, 128, 255)
		do
			local lgp = love.graphics.print
			love.graphics.print = function(text, x, y)
				local r, g, b, a = love.graphics.getColor()
				love.graphics.setColor(0, 0, 0, a)
				lgp(text, x + 1, y + 1)
				love.graphics.setColor(r, g, b, a)
				lgp(text, x, y)
			end
		end
	end,

	update = function(self, dt)
		local mx, my = love.mouse.getPosition()
		local gx, gy = math.floor(mx / 16), math.floor(my / 16)
		if not self.gui.display then
			self.cursor.x = gx * 16
			self.cursor.y = gy * 16

			self.gui.x, self.gui.y = mx, my --update gui position
		end

		--paint tiles
		if self.paintMode and (self.previousTile[1] ~= gx or self.previousTile[2] ~= gy) then
			local tile = self.map.tiles[gx + 1][gy + 1]
			tile.quadRef = self.paintTile
			tile:setProperties(self.settings)
			self.map:createBatch()
			if self.metaMap then
				self.metaMap = self:buildMetaMap(self.metaProp)
			end
			self.previousTile = {gx, gy}
			collectgarbage()
		end

		--update log
		if next(self.log) then
			for i = # self.log, 1, -1 do
				entry = self.log[i]
				entry.timeLeft = entry.timeLeft - 100 * dt
				if entry.timeLeft <= 0 then
					--table.insert(self.logHistory, entry)
					table.remove(self.log, i)
				end
			end
		end
	end,

	draw = function(self)
		love.graphics.scale(1)
		--draw the map
		love.graphics.setColor(255, 255, 255, 255)
		self.map:draw()

		--draw grid cursor
		local cursor = self.cursor
		love.graphics.setLineWidth(1)
		love.graphics.setColor(255, 0, 255, 255)
		love.graphics.rectangle("line", cursor.x, cursor.y, 16, 16)

		if self.selected then
			local selected = self.selected
			love.graphics.setLineWidth(1)
			love.graphics.setColor(255, 0, 0, 255)
			love.graphics.rectangle("line", selected.x, selected.y, 16, 16)
		end

		--draw metamap
		if self.metaMap then
			for k, v in ipairs(self.metaMap) do
				for i, j in ipairs(v) do
					if j == "Y" or j == 2 then
						love.graphics.setColor(200, 100, 100, 255)
					else
						love.graphics.setColor(100, 200, 100, 255)
					end
					local k = k - 1
					local i = i - 1
					love.graphics.print(j, k * 16 + 4, i * 16 + 2)
				end
			end
		end

		--draw gui popup
		if self.gui.display then
			local gui = self.gui
			local pad = gui.pad
			local width, height = self.tileImg:getWidth(), self.tileImg:getHeight()
			love.graphics.setColor(20, 20, 20, 200)
			love.graphics.rectangle("fill", gui.x, gui.y, gui.w, gui.h) --background
			love.graphics.setColor(255, 255, 255, 255)
			love.graphics.print("Menu", gui.x + pad, gui.y + pad)
			--tile selection
			love.graphics.rectangle("line", gui.x + pad, gui.y + pad + 16, width, height)
			love.graphics.draw(self.tileImg, gui.x + pad, gui.y + 16 + pad)
			love.graphics.print("Current:", gui.x + width + pad + 3, gui.y + 16 + pad)
			love.graphics.draw(self.tileImg, self.map.tileSheet.quads[self.paintTile.x][self.paintTile.y], gui.x + width + pad + 54, gui.y + 14 + pad)
			--tile attributes
			for k, v in ipairs(gui.widget) do
				local printValue
				local setting = self.settings[v.value]
				if type(setting) == "boolean" then
					printValue = setting == true and "Yes" or "No"
				else
					printValue = tostring(setting)
				end
				love.graphics.print(v.text .. printValue, gui.x + v.x, gui.y + v.y)
			end

		end

		--draw text log
		for k, entry in ipairs(self.log) do
			love.graphics.setColor(255, 255, 255, entry.timeLeft <= 255 and entry.timeLeft or 255)
			love.graphics.print(entry.text, 1, 16 * (k - 1))
		end

					love.graphics.setColor(255, 100, 100, 255)
		love.graphics.print(collectgarbage("count")*1024 .. " " .. love.timer.getFPS(), 1, 465)
		love.graphics.print(self.cursor.x / 16 .. " / " .. self.cursor.y / 16, 590, 465)
	end,

	logMsg = function(self, text)
		table.insert(self.log, {text = text, timeLeft = 500})
	end,

	saveMap = function(self)
		if self.mapList[self.currentMapID] ~= "blank.map" then
			local success = self.map:saveToFile(self.mapList[self.currentMapID])
			local msg = success and "Map saved." or "Save failed!"
			self:logMsg(msg)
		else
			self:logMsg("blank.map cannot be overwritten.")
		end
	end,

	keypressed = function(self, key, unicode)
		if key == "tab" then
			local gui = self.gui
			gui.display = not gui.display
			local w, h = love.graphics.getWidth(), love.graphics.getHeight()
			gui.x = gui.x + gui.w > w and w - gui.w - 4 or gui.x
			gui.y = gui.y + gui.h > h and h - gui.h - 4 or gui.y
		elseif key == "s" and modKey() then --SAVE MAP
			self:saveMap()
		elseif key == "n" and modKey() then --NEW MAP
			self:saveMap()
			local newName = tostring(#self.mapList + 1) .. ".map"
			table.insert(self.mapList, newName)
			self.map = Map(self.currentMapID .. ".map")
			self.currentMapID = #self.mapList
			self.map:createBatch()
			self:logMsg("Map " .. self.mapList[self.currentMapID] .. " created.")
		elseif key == "down" then --UP A FLOOR
			self:saveMap()
			self.currentMapID = math.max(self.currentMapID - 1, 1)
			self.map = Map(self.mapList[self.currentMapID])
			self.map:createBatch()
			self:logMsg("Map " .. self.mapList[self.currentMapID] .. " loaded.")
		elseif key == "up" then --DOWN A FLOOR
			self:saveMap()
			self.currentMapID = math.min(self.currentMapID + 1, #self.mapList)
			self.map = Map(self.mapList[self.currentMapID])
			self.map:createBatch()
			self:logMsg("Map " .. self.mapList[self.currentMapID] .. " loaded.")
		elseif key == "kp0" then
			self.metaMap, self.metaProp = nil, nil
		elseif key == "kp1" then
			self.metaMap, self.metaProp = self:buildMetaMap("solid")
			self:logMsg("Showing solid")
		elseif key == "kp2" then
			self.metaMap, self.metaProp = self:buildMetaMap("solidProjectile")
			self:logMsg("Showing solidProjectile")
		elseif key == "kp3" then
			self.metaMap, self.metaProp = self:buildMetaMap("layer")
			self:logMsg("Showing layer")
		elseif key == "kp4" then
			self.metaMap, self.metaProp = self:buildMetaMap("stairs")
			self:logMsg("Showing stairs")
		elseif key == "kp5" then
			self.metaMap, self.metaProp = self:buildMetaMap("hole")
			self:logMsg("Showing hole")
		elseif key == "return" then
			self.changeState("world")
		end

		local keyNum = tonumber(key)
		if keyNum and keyNum <= 5 and keyNum > 0 then --Toggle properties
			local prop = self.gui.widget[keyNum] 
			prop:click(self.settings)
			self:logMsg(prop.text .. tostring(self.settings[prop.value]))
		end

	end,

	keyreleased = function(self, key)
		if key == "tab" then self.gui.display = nil end
	end,

	mousepressed = function(self, x, y, button)
		local gui = self.gui
		local pad = gui.pad
		if button == "l" then
			--menu stuff
			local mouseOverMenu = inside(x, y, gui.x, gui.y, gui.w, gui.h)
			if gui.display and not mouseOverMenu then
				gui.display = not gui.display
			elseif not gui.display then
				self.selected = {x = math.floor(x / 16) * 16, y = math.floor(y / 16) * 16}
			elseif gui.display then
				--tile selection
				if inside(x, y, gui.x + pad, gui.y + pad + 16, self.tileImg:getWidth(), self.tileImg:getHeight()) then
					local newX = math.floor((x - gui.x + pad) / 18) + 1
					local newY = math.floor((y - gui.y + pad) / 18)
					if newX < 1 or newX > 4 then newX = 1 end
					if newY < 1 or newY > 12 then newY = 1 end
					self.paintTile = {x = newX, y = newY}
				else
				--property selection
					for k, v in ipairs(gui.widget) do
						if inside(x, y, gui.x + v.x, gui.y + v.y, 75, 15) then
							v:click(self.settings)
						end
					end
				end

			end
			--painting
			if love.keyboard.isDown("lgui") or love.keyboard.isDown("lctrl") then
				self.paintMode = true
			end
		elseif button == "r" then
			local gx, gy = math.floor(x / 16), math.floor(y / 16)
			local tile = self.map.tiles[gx + 1][gy + 1]
			self.paintTile = tile.quadRef
		end

	end,

	mousereleased = function(self, x, y, button)
		if button == "l" and self.paintMode then self.paintMode = nil end
		self.previousTile = {}
	end,

	buildMetaMap = function(self, property)
		local metaMap = {}
		for i, j in ipairs(self.map.tiles) do
			metaMap[i] = {}
			for k, v in ipairs(self.map.tiles[i]) do
				local p
				if type(v[property]) == "boolean" then
					p = v[property] == true and "Y" or "N"
				else
					p = v[property]
				end
				metaMap[i][k] = p
			end
		end
		return metaMap, property
	end
}
