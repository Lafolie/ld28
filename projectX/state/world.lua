return {
	reset = function(self, mute)
		love.graphics.setBackgroundColor(100, 150, 200, 255)
		love.math.setRandomSeed(os.time())
		 love.math.random()
		  love.math.random()
		love.window.setTitle("Boomerang Battle")
		print "main game"
		love.mouse.setGrabbed(true)
		self.cursor = love.mouse.newCursor("gfx/crosshair.png", 7, 8)
		love.mouse.setCursor(self.cursor)
		
		
		self.level = 1
		self.boomerangFloor = 1
		
		self.map = Map("1.map")
		self.playerTileSheet = TileSheet("gfx/player.png",16)
		self.player = Player({x=328,y=416},self.playerTileSheet,self.map)
		self.ai = AI(self.map,self.player,self.level)
		self.particles = {}
		self.particle = Particle({x = 247, y = 254}, "gfx/flame.png", "alpha", 2.5, math.rad(-90), 10, 1.25, 0.1, 1, 10, 0.5, 0)

		--music
		if not mute then
			self.bgm = cache.music("bgm/diger.ogg")
			self.bgm:setLooping(true)
			self.bgm:setVolume(0.50)
			self.bgm:play()
			self.bossMusic = cache.music("bgm/boss.ogg")
			self.bossMusic:setVolume(0.75)
		end

		--ambience
		self.ambientTrack = cache.music("sfx/ambience.ogg")
		self.ambientTrack:setLooping(true)
		self.ambientTrack:setVolume(0.75)
		self.ambientTrack:play()
		
		self.flames = self.map:getFlamesTiles()
		self.heart = cache.image("gfx/heart.png")
		
		self.floorsCompleted = 0
		
	end,

	switchMap = function(self,direction)
		
		if direction == "up" then
			if self.level > self.floorsCompleted then
				self.floorsCompleted = self.level
			end
			self.level = self.level +1
		elseif direction == "down" then
			self.level = self.level -1
		end
		print(self.level)
		print(self.boomerangFloor)
		
		if self.boomerangFloor == self.level then
			self.player.boomerang.onCurrentLevel = true
		end
		if self.player.HasBoomerang then
			self.boomerangFloor = self.level
		end
		self.map = Map(tostring(self.level)..".map")
		self.player.map = self.map
		self.player.boomerang.map = self.map
		self.ai = AI(self.map,self.player,self.level)
		self.flames = self.map:getFlamesTiles()
		self.particles = {}
		if self.level <= self.floorsCompleted then  -- bug fix stops stairs from locking behind you when decending
			for i = #self.flames, 1, -1 do
				table.insert(self.particles,Particle({x = self.flames[i].x , y = self.flames[i].y-5}, "gfx/flame.png", "alpha", 2.5, math.rad(-90), 10, 1.25, 0.1, 1, 10, 0.5, 0))
			end
			self.flames = {}
		end
		if self.level == 10 then
			self.bgm:stop()
			self.bossMusic:play()
		else
			self.bossMusic:stop()
			self.bgm:play()
		end
		
	end,
	
	recordBoomerangFloor = function(self)
		self.boomerangFloor = self.level-1
	end,
		
		
	update = function(self, dt)
		local pos =	self.player.boomerang.position
		for i = #self.flames, 1, -1 do
			local vector = {x=self.flames[i].x -pos.x,y=self.flames[i].y -pos.y}
			local distance = math.sqrt((vector.x)^2 + (vector.y)^2)
			if distance < 30 then
			print("flame")
				table.insert(self.particles,Particle({x = self.flames[i].x , y = self.flames[i].y-5}, "gfx/flame.png", "alpha", 2.5, math.rad(-90), 10, 1.25, 0.1, 1, 10, 0.5, 0))
				table.remove(self.flames,i)
			end
		end
		
		if self.player.hp == 0 then
			self.bgm:stop()
			self.ambientTrack:stop()
			self.bossMusic:stop()
			self.changeState("init")
		end

		if self.player.recordBoomerangPosition == true then
			self:recordBoomerangFloor(self)
			self.player.recordBoomerangPosition = false
		end
		if self.player.switchMap == true then
			if #self.flames == 0 or self.player.lastStairs == "down" then  										-- bug fix can not go down a floor without needing to trigger tourches 
				if self.player.HasBoomerang == false and self.boomerangFloor == self.level then		--bug fix stops boomerang from disapearing when atempting to use locked staircase
					self.player.boomerang.onFloor = true
					self.player.boomerang.onCurrentLevel = false
					self.player.boomerang.velocity.x = 0
					self.player.boomerang.velocity.y = 0
					self.player.boomerang.acceleration.x = 0
					self.player.boomerang.acceleration.y = 0
				end
			self:switchMap(self.player.lastStairs)
			end
			
			self.player.switchMap = false
		end
		
		self.player:update(dt)
		self.ai:update(dt)
		for j,x in ipairs(self.particles) do
			x:update(dt)	
		end
	end,

	draw = function(self)
		love.graphics.scale(game.scale)
		self.map:drawBatch1()
		if #self.flames ~= 0 then
			self.map:hideStairs()
		end
		self.ai:draw()
		self.player:draw()
		for j,x in ipairs(self.particles) do
			x:draw()	
		end
		self.map:drawBatch2()
		love.graphics.setColor(255, 255, 255, 255)
		for hp = 0,  self.player.hp - 1 do
			love.graphics.draw(self.heart, 10 * hp + 4, 4)
		end
		-- love.graphics.print(love.timer.getFPS() .. "fps", 1, 16)
	end,
	mousepressed = function(self, x, y, button)
		self.player:mousepressed(x, y, button)
	end,

	keypressed = function(self, key, unicode)
		if key == "tab" then
			game.scale = game.scale == 2 and 1 or 2
			love.window.setMode(640 * game.scale, 480 * game.scale)
		end
	end
}